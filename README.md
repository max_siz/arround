# ARROUND TDE smart contract
![Smart contracts deployed on Rinkeby test network](https://img.shields.io/badge/Rinkeby-deployed-green.svg)  
![Smart contracts deployed on Mainnet network](https://img.shields.io/badge/Mainnet-deployed-green.svg)  
Etherscan Rinkeby links:  
[Arroundtoken v2](https://rinkeby.etherscan.io/token/0x0fc6a8d9d987dd3b6602a24c787d010c4b5e9e63)   
Etherscan MainNet links:  
[Arroundtoken](https://etherscan.io/address/0xcb089b8ae76b5df461d40e957603f7a59aea9e0d)

* [General](#general)
* [Special metods(functions)](#contracts-special-metods-review)
* [Testing](#testing)
* * [Testing log 20181201 - for Download](20181201_testlog.html)
* * [Testing log 20181204 - for Download](20181204_testlog.html)
* [Deploy](#deploy)
* [Arround project  WP (ru)](https://arround.io/content/file/arround-whitepaper-ru.pdf)

## General
  Information system consist of one main smart contracts: ERC20 - compatible token processing **Arroundtoken**. All tokens intended for distribution are emitted at the time of Arroundtoken smart contract deployment. After deploy all this tokens keeped on __special wallets__ in an agreed proportion:  
  * _accTDE_ - tokens for Pre-sale,  Token-Sale and Sale-Bonus tokens;
  * _accFoundCDF_  - CDF found tokens;
  * _accFoundNDF1_ - NDF found tokens, part 1, 50%, unfrozen;
  * _accFoundNDF2_ - NDF found tokens, part 2, 35%, will be unfrozen after 1 year;
  * _accFoundNDF3_ - NDF found tokens, part 3, 15%, will be unfrozen after 2 years;
  * _accTeam_ - tokens for team, will be unfrozen after 1 year;  
  * _accBounty_ - tokens for bounty reward; 


## Contracts special metods review  

#### Transfer ownership
Two-step-change-ownership pattern is implemented, where the ownership needs to be claimed.  
Current owner (with its key)  send  0 ETH transaction with transferOwnership(newOwner), where newOwner is address of new owner.  
After that new owner must send  0 ETH transaction with claimOwnership(). The ownership will be finally changed  when   this  second transaction be confirmed. 

####  reclaimToken(token)
Contract owner can claim any tokens that transfered to this contract address.
  
#### Token distribution from Cab
In addition to standard ERC20 methods, CIX100  smart contract support `multiTransfer` method
for batch token distribution from CAB. This allows spend much less Gas and Time for this operation.  
Method recieve two arrays as params:  
- array of addresses;  
- array of amounts for each address;  
Under the terms of the campaign, this method available just for initial token keeper accounts:accTDE and accBounty.  
Method verify:  
- number of adress array elements >0 and <=255 (due to Gas Limit per block);  
- length of two arrays are equal;  
- each element in array of addresses is not zero address;  
- sum of all elements in  array of amounts not more then sender balance of tokens;  
**!!! Address validation must be provided by the caller!!!**  

Method emit events:  
`Transfer` - on each transfer in the batch;  
`BatchDistrib` - with count and sum all transfers in the batch.  
**!!! It's strongly recommended for smart contract operator to catch and analysis these events for prevent token distribution errors.**

## Testing
  Unit tests  have designed with Truffle framework. Please, use [original recommendation](https://truffleframework.com/docs) for Truffle/node install.   
    
    __!!! Atention !!!__
        Please check your truffle version befor testing

```bash
    $ truffle version
Truffle v4.1.14 (core: 4.1.14)
Solidity v0.4.24 (solc-js)
``` 
It's latest stable Truffle (and solc-js) version on the moment of smart contarct develop. That why one need replace two strings of smart contract code **for run tests**.
Original string 1 `pragma solidity 0.5.0;` must be replaced with `pragma solidity 0.4.24;`.
Original string 440 `function multiTransfer(address[] calldata  _investors, uint256[] calldata   _value )`  must be replaced with `function multiTransfer(address[]   _investors, uint256[]    _value )`.

  Truffle   

## Deploy
### PreDeploy
There are 8 separate addresses used for these smart contracts. Please
Prepare 8 Ethereum accounts:  
1. _Owner_ - deploy smart contracts from this address  and  use for call onlyOwner methods (see source code);
2. _accTDE_ - tokens for Pre-sale,  Token-Sale and Sale-Bonus tokens;
3. _accFoundCDF_ - NDF found tokens;
4. _accFoundNDF1_ - CDF found tokens, part 1, 50%, unfrozen;
5. _accFoundNDF2_ - CDF found tokens, part 2, 35%, will be unfrozen after 1 year;
6. _accFoundNDF3_ - CDF found tokens, part 3, 15%, will be unfrozen after 2 years;
7. _accTeam_ - tokens for team, will be unfrozen after 1 year;  
8. _accBounty_ - tokens for bounty reward;  
Please, keep secret keys very safely !!!  

### Deploy
1. Deploy **Arroundtoken** smart  contract. Specify the appropriate adresses and totalSupply token amount in the constructor parameters.  


------------
Keys used  in Rinkeby contract
```
0 - Owner
384d9719f2cdfa068a58811541aa1a6059306a4ae61a0a360ee6443d3f610977
0xafB42ffDC859f82eDb3E93680F95212200f0CCA1

1 - accTDE
ab8a6e5b3404688151a9ef9d5a1adf2121aecc69d78decb10a7fa69b3da2a5e2
0x86C3582b6505CcB8faDAcb211fC1E5a8fDD26E91

2 - accFoundCDF
3b1fc368952b1dde285982931aa2f15722e8b93288aed942628d8c8ce6843cb8
0x919DFdc25B5239b2B8F2F21607E6dd83a95a8185

3 - accFoundNDF1
646734ad0505dc57652e936f5562a4722177507729c9f74e6c33d3fdcf73def5
0x834e862Bf84892937e9eD35D326a54f6c224fabE

4 - accFoundNDF2
83d33cf58e85d1adaea926fca66b3c99b54d37ef6729f1972b4a60a841a68273
0x1f4367bFA923543A88D609d5cCe9ffE29EC4584e

5  - accFoundNDF3
bba0e19d2911c2ca774c2a47c1c10c004ac4d902f65183e8992f4584c05291e0
0xa0363Cd6fC7A1BD68D2e330178B19eb6F7642F13

6  - accTeam
0b74d3de2d9841498807113585645dbd28e825f9711e1e83df99c9cc5377b11d
0x950260F0ffbE71b61ABb24D15d7162E1710fBaeF

7 - accBounty
4c4d206a9440fd15a7eb0a33ec3c8e83a3bfa295a5da67d353b6efea7f7fc409
0x4cB856C3b3F7F8ED6f9cdDb2F9827De32CB45241

```
Save logs
```bash
# Install aha
sudo apt-get install aha

#Run  test
truffle test |aha --black --title 'Arroundtoken test logs:20181201' >  20181201_testlog.html

```
### Ethereum Main net addresses

* 0xcD82df3A89cA9BaE00E7FCbc63AE1ce363b62E09 - 0.1 ETH -will be new  owner
* __accFoundСDF__ 0x934D72539b7796Bb3a9eE7d82815981b4fdb3bc1
* __accTDE__ 0xd5e098c0cD57628F94cE738ed92538f6bAB2F602
* __accTeam__ 0x5ecC841315aB512D0aaA390DCBfD2f0dF21cA750
* __accFoundNDF1__ 0xF8f0Ca069e3f3207Df5f7D4BA5668bCd17688bb1
* __accFoundNDF2__ 0xa070C62c307F3523CCAF9C5aA3eAFDF4C4808c8d
* __accFoundNDF3__ 0x7B9e47d08685c792DA1B9c1826c0C4AeD73Fa095
* __accBounty__ 0xd8db91efaf41be13449b668dff1efada744cb81a
