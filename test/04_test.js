var Arroundtoken = artifacts.require("./Arroundtoken.sol");
var MyERC20;
var gasUsedwei=0;
var gasUsed=0;
var _totalSupply;
var event;
var event1;
var owner_old;
var owner_new;
var cnt;
var AccTDE;
var amount_transfer=1000;
var balanceAcc8Before;
var balanceAcc0Before;
var balanceAcc8After;
var balanceAccTDEBefore;
var allowance_before;


contract('Arroundtoken functionality tests - 04',async (accounts) => {


      // body...
        //acc[1], //accTDE
        //acc[2], //_accFoundCDF
        //acc[3], //_accFoundNDF1
        //acc[4], //_accFoundNDF2
        //acc[5], //_accFoundNDF3
        //acc[6], //_accTeam
        //acc[7], //_accBounty

    before( function() {
        return Arroundtoken.deployed().then(function(instance){
            MyERC20 = instance;
            //have got count of tokens issue
            return MyERC20.totalSupply(); 
        }).then(function(callResult){
            _totalSupply=callResult;
            return MyERC20.accTDE.call();
        }).then(function(callResult){
            AccTDE=callResult;
            return true;
        });
    });


describe('Account tries to give the allowance to account 0'
                +'\r\n Expected behaviour: The attempt is failed. The allowance is not gotten', function(){
        it('04.01_to give the allowance_to account 0', function() {

          return MyERC20.approve('0x0000000000000000000000000000000000000000', amount_transfer, 
                                    {from: accounts[2], gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                        console.log("ERROR! - OK. The allowance is not gotten.", err.toString());
              
            }).then(function(txResult){
                return true;
            });
        });

        it('04.02_to_increase the allowance_to account 0', function() {

          return MyERC20.increaseAllowance('0x0000000000000000000000000000000000000000', amount_transfer, 
                                    {from: accounts[2], gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                        console.log("ERROR! - OK. The allowance is not gotten.", err.toString());
              
            }).then(function(txResult){
                return true;
            });
        });

        it('04.03_to_decrease the allowance_to account 0', function() {

          return MyERC20.decreaseAllowance('0x0000000000000000000000000000000000000000', amount_transfer, 
                                    {from: accounts[2], gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                        console.log("ERROR! - OK. The allowance is not gotten.", err.toString());
              
            }).then(function(txResult){
                return true;
            });
        });
    });

    
    describe('Account tries to give the allowance to account. Account of spender is not 0. Account of owner is not frozen'
                +'\r\n Expected behaviour: The attempt is successful. The allowance is gotten', function(){

            it('04.04_to approve_8_to_9', function() {
        
                return MyERC20.allowance(accounts[8], accounts[9]).then(function(callResult){
                    //console.log('allowance_before',callResult);
                    allowance_before = callResult.toNumber();
                    return MyERC20.approve(accounts[9], amount_transfer, {from: accounts[8], gasPrice: web3.toWei(1,"Gwei")});
                }).then(function(txResult){
                    event=txResult.logs.find(e => e.event === 'Approval').event;
                    return MyERC20.allowance(accounts[8], accounts[9]);
                }).then(function(callResult){
                    //console.log('allowance_after',callResult);
                    assert.equal(callResult.minus(allowance_before),amount_transfer, 'ERROR: the allowance has to be granted!');
                });
            });

            it('04.04.01_to approve_8_to_9_Check_event', function() {
                assert.equal(event,'Approval', "ERROR: the event has to be @Approval@");
                       
            });

            it('04.05_to increase allowance_8_to_9', function() {
        
                return MyERC20.allowance(accounts[8], accounts[9]).then(function(callResult){
                   // console.log('allowance_before',callResult);
                    allowance_before = callResult.toNumber();
                    return MyERC20.increaseAllowance(accounts[9], 10, {from: accounts[8], gasPrice: web3.toWei(1,"Gwei")});
                }).then(function(txResult){
                    event=txResult.logs.find(e => e.event === 'Approval').event;
                    return MyERC20.allowance(accounts[8], accounts[9]);
                }).then(function(callResult){
                    //console.log('allowance_after',callResult);
                    assert.equal(callResult.minus(allowance_before),10, 'ERROR: the allowance has to be increased');
                });
            });

            it('04.05.01_to increase allowance_8_to_9_Check_event', function() {
                assert.equal(event,'Approval', "ERROR: the event has to be @Approval@");
                       
            });

            it('04.06_to decrease allowance_8_to_9', function() {
        
                return MyERC20.allowance(accounts[8], accounts[9]).then(function(callResult){
                    //console.log('allowance_before',callResult);
                    allowance_before = callResult.toNumber();
                    return MyERC20.decreaseAllowance(accounts[9], 20, {from: accounts[8], gasPrice: web3.toWei(1,"Gwei")});
                }).then(function(txResult){
                    event=txResult.logs.find(e => e.event === 'Approval').event;
                    return MyERC20.allowance(accounts[8], accounts[9]);
                }).then(function(callResult){
                    //console.log('allowance_after',callResult);
                    assert.equal(callResult.add(20).minus(allowance_before),0, 'ERROR: the allowance has to be decreased');
                });
            });

            it('04.06.01_to decrease allowance_8_to_9_Check_event', function() {
                assert.equal(event,'Approval', "ERROR: the event has to be @Approval@");
                       
            });
        });

    describe('Account has gotten the tokens. Then spender of account tries call method @transferFrom@ in some cases when the transfer has not to happen'
                +'\r\n Account of owner is not frozen. Expected behaviour: The attempts is failed. The transfers have not to happen', function(){
            it('04.07_transfer_from_AccTDE_account_to_other_account', function() {
  
  
                return MyERC20.balanceOf.call(accounts[8]).then(function(callResult){
                    //console.log('balanceAcc8Before',callResult.toNumber());
                    balanceAcc8Before = callResult.toNumber();
                    return MyERC20.balanceOf.call(AccTDE);
                }).then(function(callResult){
                    //console.log('balanceAccTDEBefore',callResult.toNumber());
                    balanceAccTDEBefore = callResult.toNumber();
                    return MyERC20.transfer(accounts[8],amount_transfer,{from: AccTDE, gasPrice: web3.toWei(1,"Gwei")});
                }).then(function(txResult){
                    return MyERC20.balanceOf(accounts[8]);
                }).then(function(callResult){
                    balanceAcc8After=callResult;
                   // console.log('balanceAcc8After=', callResult);
                    assert.equal(callResult.minus(balanceAcc8Before)
                            ,amount_transfer, 'ERROR  of   token transfer');
                });   
            }); 

            it('04.08_transferFrom_from_acc8_to account 0', function() {
                return MyERC20.transferFrom(accounts[8],'0x0000000000000000000000000000000000000000',amount_transfer,{from: accounts[9], 
                                gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                    console.log("ERROR! - OK. The transfer has not happened.", err.toString());
                  
                }).then(function(txResult){
                    return MyERC20.balanceOf(accounts[8]);

                }).then(function(callResult){
                   // console.log('balanceAcc8After=', callResult);
                    
                    assert.equal(callResult.minus(balanceAcc8After),0 
                            , 'ERROR: the balance is changed!');
                });   
            });

            it('04.09_transferFrom_from_acc8_to other account. The amount of  the transfer  is more  than there is the allowance amount ', function() {
                return MyERC20.allowance(accounts[8], accounts[9]).then(function(callResult){
                    //console.log('allowance_before=',callResult);
                    //console.log('amount_transfer=',amount_transfer);

                    return MyERC20.transferFrom(accounts[8],accounts[0],amount_transfer,{from: accounts[9], 
                                gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                    console.log("ERROR! - OK. The transfer has not happened.", err.toString());
                    });
                }).then(function(txResult){
                    return MyERC20.balanceOf(accounts[8]);

                }).then(function(callResult){
                    //console.log('balanceAcc8After=', callResult);
                    assert.equal(callResult.minus(balanceAcc8After),0 
                          , 'ERROR: the balance is changed!');
                });   
            });


            it('04.10_transferFrom_from_acc8_to other account. The amount of the transfer  is more than there is the balance amount ', function() {
                return MyERC20.increaseAllowance(accounts[9], 20, {from: accounts[8], gasPrice: web3.toWei(1,"Gwei")}).then(function(txResult){
                    return MyERC20.allowance(accounts[8], accounts[9]);
                }).then(function(callResult){    
                    //console.log('allowance_before!!!!=',callResult);
                    //console.log('amount_transfer=',amount_transfer);

                    return MyERC20.transferFrom(accounts[8],accounts[0],amount_transfer+10,{from: accounts[9], 
                                gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                    console.log("ERROR! - OK. The transfer has not happened.", err.toString());
                    });
                }).then(function(txResult){
                    return MyERC20.balanceOf(accounts[8]);

                }).then(function(callResult){
                  //  console.log('balanceAcc8After=', callResult);
                    assert.equal(callResult.minus(balanceAcc8After),0 
                           , 'ERROR: the balance is changed!');
                });   
            });
    });

    describe('Account has gotten the tokens. Then spender of account tries call method @transferFrom@  when the transfer has to happen'
                +'\r\n Account of owner is not frozen. Expected behaviour: The attempt is successful. The transfer has to happen', function(){

            it('04.11_transferFrom_from_acc8_to other account. There are all conditions to happen the transfer', function() {

                return MyERC20.allowance(accounts[8], accounts[9]).then(function(callResult){    
                    //console.log('allowance_before_______________=',callResult);
                    allowance_before=callResult;
                    return MyERC20.balanceOf(accounts[8]);
                 }).then(function(callResult){
                    //console.log('balanceAcc8Before=',callResult);
                    balanceAcc8Before=callResult;
                    return MyERC20.balanceOf(accounts[0]);
                 }).then(function(callResult){
                    //console.log('balanceAcc0Before=', callResult);
                    balanceAcc0Before=callResult;
                    return MyERC20.transferFrom(accounts[8],accounts[0],100,{from: accounts[9], 
                                gasPrice: web3.toWei(1,"Gwei")});
                }).then(function(txResult){
                    event=txResult.logs.find(e => e.event === 'Transfer').event;
                    return MyERC20.balanceOf(accounts[8]);

                }).then(function(callResult){
                    //console.log('balanceAcc8After=', callResult);
                    assert.equal(callResult.add(100).minus(balanceAcc8Before),0 
                           , 'ERROR  of   token transfer');
                });   
            });


            it('04.11.1_transferFrom_from_acc8_to other account. There are all conditions to happen the transfer. Check event', function() {
                assert.equal(event,'Transfer', "the event has to be @Transfer@");
            });

            
            it('04.11.2_transferFrom_from_acc8_to other account. There are all conditions to happen the transfer. Check balance of account0', function() {
                     return MyERC20.balanceOf(accounts[0]).then(function(callResult){    
                     //console.log('balanceAcc0After=',callResult);
                     //console.log('balanceAcc0Before=',balanceAcc0Before);
                    
                    assert.equal(callResult.minus(balanceAcc0Before),100, "ERROR  of   token transfer");
                   return true;
                });
            });

            it('04.11.3_transferFrom_from_acc8_to other account. There are all conditions to happen the transfer. Check allowance', function() {
                     return MyERC20.allowance(accounts[8], accounts[9]).then(function(callResult){    
                    //console.log('allowance_after______________=',callResult);
                    //console.log('allowance_before______________=',allowance_before);
                    //return true;
                    assert.equal(callResult.add(100).minus(allowance_before), 0, "ERROR  of   token transfer via call of method @transferFrom@");
                });
            });


    });


});







                  



   

