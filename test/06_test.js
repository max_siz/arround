const { latestTime } = require('./helpers/latestTime');
const { increaseTimeTo, duration } = require('./helpers/increaseTime');

var Arroundtoken = artifacts.require("./Arroundtoken.sol");
var MyERC20;
var gasUsedwei=0;
var gasUsed=0;
var _totalSupply;
var _TDE_FINISH;
var date_block;
var amount_transfer=10;
var balanceAccTeamBefore;
var balanceAccTeamAfter;
var balanceAccFoundNDF2Before;
var time_for_new_date1=365; //first time we inscrease block date by 365 day
var time_for_new_date2=365; //second time we inscrease block date by 365 day
var event;
var event1;
var _owner;
var allow_before;
var allow_amount=1000;

var AccTeam;
var AccFoundNDF2
var AccFoundNDF3
var balanceAcc8Before;
var balanceAcc0Before;
var balanceAccFoundNDF3Before;
var balanceAccTeamBefore;


contract('Arroundtoken functionality tests - 06',async (accounts) => {

    // body...
        //acc[1], //accTDE
        //acc[2], //_accFoundCDF
        //acc[3], //_accFoundNDF1
        //acc[4], //_accFoundNDF2
        //acc[5], //_accFoundNDF3
        //acc[6], //_accTeam
        //acc[7], //_accBounty

    before( function() {
        return Arroundtoken.deployed().then(function(instance){
            MyERC20 = instance;
            //have got count of tokens issue
            return MyERC20.totalSupply(); 
        }).then(function(callResult){
            _totalSupply=callResult
            return MyERC20.TDE_FINISH.call();
        }).then(function(callResult) {
           // console.log('TDE= ', callResult.toNumber());
              _TDE_FINISH=callResult.toNumber();
             return MyERC20.accTeam.call();
        }).then(function(callResult) {
              AccTeam=callResult;
             return MyERC20.accFoundNDF2.call();
        }).then(function(callResult) {
              AccFoundNDF2=callResult;
              return MyERC20.accFoundNDF3.call();
        }).then(function(callResult) {
              AccFoundNDF3=callResult;
             return true;
        });
    });



    

    describe('Checking of tokens transfer from account after defrosting. '
                +'\r\n The sender has the balance it was frozen'
                +'\r\n Expected behaviour: The transfer is  done, the account of recipient has got the tokens,  the balances of accounts are changed.   ', function(){

            it('change_time_of_block_fot the first time', async function () { //вот тут времянку сделал. Может, излишний код, надо обсудить
                        date_block = await latestTime();
                       // console.log('date_block=', date_block);


                            await increaseTimeTo(date_block + duration.days(time_for_new_date1));
                            date_block = await latestTime();
                           // console.log('latestTime + time_for_new_date1 = ',date_block)
            });



        it('06.01_AccTeam_transfers_the tokens_to_other_account_Unfrozen_balance', function() {
                    event='';
            
                    return MyERC20.balanceOf(AccTeam).then(function(callResult){
                    //console.log('balance_AccTeam_before=',callResult);
                    balanceAccTeamBefore=callResult;
                    return MyERC20.balanceOf(accounts[0]);
                }).then(function(callResult) {
                    //console.log('balance_acc0_before=', callResult)
                    balanceAcc0Before=callResult;
                    return MyERC20.transfer(accounts[0],amount_transfer,{from: AccTeam, gasPrice: web3.toWei(1,"Gwei")})
                    
                }).then(function(txResult){
                     event=txResult.logs.find(e => e.event === 'Transfer').event;
                     return MyERC20.balanceOf(AccTeam);
                }).then(function(callResult){
                    //console.log('balance_AccTeam_After=',callResult);           
                    assert.equal(balanceAccTeamBefore.minus(callResult),amount_transfer,"ERROR: the tokens have to be unfrozen ");
                  
                });
            });

        it('06.01.1_AccTeam_transfers_to_other_account_Check_balance_of_recipient', function() {
                    
                    return MyERC20.balanceOf(accounts[0]).then(function(callResult){
                    //console.log('balance_acc0_after=', callResult);
                    //console.log('balance_acc0_before', balanceAcc0Before);
                     assert.equal(callResult-balanceAcc0Before,amount_transfer,"ERROR: he balance has to change");
                   
                });
            });
        it('06.01.2_AccTeam_transfers_to_other_account_Check_event', function() {
                    //console.log('event=', event);
                    assert.equal(event,'Transfer', "ERROR: the event has to be @Transfer@");
                   
            });

        it('06.02_AccFoundNDF2_transfers_the tokens_to_other_account_Unfrozen_balance', function() {
                    event='';
            
                    return MyERC20.balanceOf(AccFoundNDF2).then(function(callResult){
                    //console.log('balanceAccFoundNDF2Before=',callResult);
                    balanceAccFoundNDF2Before=callResult;
                    return MyERC20.balanceOf(accounts[0]);
                }).then(function(callResult) {
                    //console.log('balance_acc0_before=', callResult)
                    balanceAcc0Before=callResult;
                    return MyERC20.transfer(accounts[0],amount_transfer,{from: AccFoundNDF2, gasPrice: web3.toWei(1,"Gwei")})
                    
                }).then(function(txResult){
                     event=txResult.logs.find(e => e.event === 'Transfer').event;
                     return MyERC20.balanceOf(AccFoundNDF2);
                }).then(function(callResult){
                    //console.log('balanceAccFoundNDF2Before=',callResult);           
                   assert.equal(balanceAccFoundNDF2Before.minus(callResult),amount_transfer,"ERROR: the tokens have to be unfrozen ");
                  return true;
                  
                });
            });

        it('06.02.1_AccFoundNDF2_transfers_to_other_account_Check_balance_of_recipient', function() {
                    
                    return MyERC20.balanceOf(accounts[0]).then(function(callResult){
                    //console.log('balance_acc0_after=', callResult);
                    //console.log('balance_acc0_before', balanceAcc0Before);
                     assert.equal(callResult-balanceAcc0Before,amount_transfer,"ERROR: the balance has to change");
                   
                });
            });
        it('06.02.2_AccFoundNDF2_transfers_to_other_account_Check_event', function() {
                    //console.log('event=', event);
                    assert.equal(event,'Transfer', "ERROR: the event has to be @Transfer@");
                   
            });
    });

    describe('Checking of tokens transfer from AccFoundNDF3 during frozen time period.'
                    +'\r\n Expected behaviour: The attempts is failed. The transfers have not to happen'
                    +'\r\n  the balances of accounts are not changed.', function(){

                
                it('06.03.1_transfer_from_AccFoundNDF3(it is frozen)_to_other_account', function() {
                        
                           return MyERC20.balanceOf(AccFoundNDF3).then(function(callResult){
                            //console.log('balance_AccFoundNDF3_before=',callResult);
                            balanceAccFoundNDF3Before=callResult;
                            return MyERC20.balanceOf(accounts[0]);
                        }).then(function(callResult) {
                            //console.log('balance_acc0_before=', callResult)
                            balanceAcc0Before=callResult;
                            return MyERC20.transfer(accounts[0],amount_transfer,{from: AccFoundNDF3, gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                            console.log("ERROR! - OK, the transfer has not happened, it is the frozen time", err.toString())
                            });
                        }).then(function(txResult){
                             return MyERC20.balanceOf(AccFoundNDF3);
                        }).then(function(callResult){
                             //console.log('balance_accFoundNDF3_After=',callResult);           
                            assert.equal(callResult.minus(balanceAccFoundNDF3Before),0,"ERROR: the tokens has to be frozen ");
                            //return true;
                          
                        });
                    });

                it('06.03.2_transfer_from_AccFoundNDF3(it is frozen)_to_other_account_Check_balance_of_recipient', function() {
                            
                            return MyERC20.balanceOf(accounts[0]).then(function(callResult){
                            //console.log('balance_acc0_after=', callResult);
                            //console.log('balance_acc0_before', balanceAcc0Before);
                             assert.equal(callResult-balanceAcc0Before,0,"ERROR: the balance has not to change");
                           
                        });
                });
        });


    describe('Account owner of AccTeam tries to increase allowance of using his own tokens when the account of token owner is not frozen. '
                +'\r\n Expected behaviour: The try is successful. And the allowance is increased', function(){

        it('06.04.1_increase allowance after unfreezing', function() {
                    event='';
                    return MyERC20.allowance(AccTeam, accounts[0]).then(function(callResult){
                    //console.log('allow_before=', callResult);
                    allow_before=callResult;
                    return MyERC20.increaseAllowance(accounts[0], 10 , {from: AccTeam, gasPrice: web3.toWei(1,"Gwei")});
                    }).then(function(txResult){
                        event=txResult.logs.find(e => e.event === 'Approval').event;
                        return MyERC20.allowance(AccTeam, accounts[0]);
                    }).then(function(callResult){
                      //console.log('allow_after=', callResult);
                    assert.equal(callResult.minus(allow_before),10, "ERROR: the allowance has to change");
                   
                });
            });

        it('06.04.2_increase allowance after unfreezing_Check_event', function() {
                //    console.log('event=', event);
                    assert.equal(event,'Approval', "ERROR: the event has to be @Approval@");
                   
            });
    });

    describe('Account owner of AccFoundNDF2 tries to increase allowance of using his own tokens when the account of token owner is not frozen. '
                +'\r\n Expected behaviour: The try is successful. And the allowance is increased', function(){

        it('06.05.1_increase allowance after unfreezing', function() {
                    event='';
                    return MyERC20.allowance(AccFoundNDF2, accounts[0]).then(function(callResult){
                    //console.log('allow_before=', callResult);
                    allow_before=callResult;
                    return MyERC20.increaseAllowance(accounts[0], 10 , {from: AccFoundNDF2, gasPrice: web3.toWei(1,"Gwei")});
                    }).then(function(txResult){
                        event=txResult.logs.find(e => e.event === 'Approval').event;
                        return MyERC20.allowance(AccFoundNDF2, accounts[0]);
                    }).then(function(callResult){
                      //console.log('allow_after=', callResult);
                    assert.equal(callResult.minus(allow_before),10, "ERROR: the allowance has to change");
                   
                });
            });

        it('06.05.2_increase allowance after unfreezing_Check_event', function() {
                //    console.log('event=', event);
                    assert.equal(event,'Approval', "ERROR: the event has to be @Approval@");
                   
            });
    });


    describe('Account owner of AccFoundNDF2 tries to decrease allowance of using his own tokens when the account of token owner is not frozen.'
                +'\r\n Expected behaviour:  The try is successful. And the allowance is decreased', function(){
        it('06.06.1_decrease allowance after unfreezing', function() {
                    event='';
                    return MyERC20.allowance(AccFoundNDF2, accounts[0]).then(function(callResult){
                    //console.log('allow_before=', callResult);
                    allow_before=callResult;
                    return MyERC20.decreaseAllowance(accounts[0], 1 , {from: AccFoundNDF2, gasPrice: web3.toWei(1,"Gwei")});
                    }).then(function(txResult){
                        event=txResult.logs.find(e => e.event === 'Approval').event;
                        return MyERC20.allowance(AccFoundNDF2, accounts[0]);
                    }).then(function(callResult){
                      //console.log('allow_after=', callResult);
                    assert.equal(allow_before.minus(callResult),1, "ERROR: the allowance has to change");
                    //return true;
                   
                });
            });

        it('06.06.2_decrease allowance after unfreezing_Check_event', function() {
                   console.log('event=', event);
                    assert.equal(event,'Approval', "ERROR: the event has to be @Approval@");
                   
            });
    });


    describe('Account owner of AccFoundNDF3 tries to approve to use his own tokens when the account of token owner is frozen. '
                +'\r\n Expected behaviour: The attempt is failed. The allowance is not gotten', function(){

        it('06.07.1_approve to use the tokens during frozen time period', function() {
                   
                    return MyERC20.allowance(AccFoundNDF3, accounts[0]).then(function(callResult){
                    //console.log('allow_before=', callResult);
                    allow_before=callResult;
                    return MyERC20.approve(accounts[0], 1 , {from: AccFoundNDF3, gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                    console.log("ERROR! - OK, the approval is not given", err.toString())
                        });
                    }).then(function(){
                        return MyERC20.allowance(AccFoundNDF3, accounts[0]);
                    }).then(function(callResult){
                    //console.log('allow_after=', callResult);
                    assert.equal(allow_before.minus(callResult),0, "ERROR: the approval has not to be given");
               
            });
        });
    });

     


     describe('Account AccFoundNDF3 tries to give the allowance to account. Account of spender is not 0. The balance of the account is unfrozen'
                +'\r\n Expected behaviour: The attempt is successful. The allowance is gotten', function(){


        it('change_time_of_block_fot the_second time', async function () { //вот тут времянку сделал. Может, излишний код, надо обсудить
           
                date_block = await latestTime();

                //console.log('date_block_after_first_change=', date_block);

                await increaseTimeTo(date_block + duration.days(time_for_new_date2));
                date_block = await latestTime();
                //console.log('latestTime + time_for_new_date2 = ',date_block)
            //}
            
          });



            it('06.08_AccFoundNDF3_approves_to_0', function() {
        
                return MyERC20.allowance(AccFoundNDF3,accounts[0] ).then(function(callResult){
                    //console.log('allowance_before',callResult);
                    allow_before = callResult.toNumber();
                    return MyERC20.approve(accounts[0], amount_transfer, {from: AccFoundNDF3, gasPrice: web3.toWei(1,"Gwei")});
                }).then(function(txResult){
                    event=txResult.logs.find(e => e.event === 'Approval').event;
                    return MyERC20.allowance(AccFoundNDF3,accounts[0]);
                }).then(function(callResult){
                    //console.log('allowance_after',callResult);
                    assert.equal(callResult.minus(allow_before),amount_transfer, 'ERROR: the allowance has to be granted!');
                });
            });

            it('06.08.01_AccFoundNDF3_approves_to_0_Check_event', function() {
                assert.equal(event,'Approval', "ERROR: the event has to be @Approval@");
                       
            });

    });



     describe('Then spender of account AccFoundNDF3 tries call method @transferFrom@  when the transfer has to happen'
                +'\r\n Expected behaviour: The attempt is successful. The transfer has to happen', function(){

            it('06.09_transferFrom_from_AccFoundNDF3_to other account. There are all conditions to happen the transfer', function() {

                return MyERC20.allowance(AccFoundNDF3, accounts[0]).then(function(callResult){    
                   // console.log('allowance_before_______________=',callResult);
                    allow_before=callResult;
                    return MyERC20.balanceOf(accounts[8]);
                 }).then(function(callResult){
                    //console.log('balanceAcc8Before=', callResult);
                    balanceAcc8Before=callResult;
                    return MyERC20.balanceOf(AccFoundNDF3);
                 }).then(function(callResult){
                    //console.log('balanceAccFoundNDF3Before=', callResult);
                    balanceAccFoundNDF3Before=callResult;
                    return MyERC20.transferFrom(AccFoundNDF3,accounts[8],amount_transfer,{from: accounts[0], 
                                gasPrice: web3.toWei(1,"Gwei")});
                }).then(function(txResult){
                    event=txResult.logs.find(e => e.event === 'Transfer').event;
                    return MyERC20.balanceOf(accounts[8]);

                }).then(function(callResult){
                    //console.log('balanceAcc8After=', callResult);
                    assert.equal(callResult.minus(balanceAcc8Before),amount_transfer
                           , 'ERROR  of   token transfer');
                });   
            });


            it('06.09.1_transferFrom_from_AccFoundNDF3_to other account. There are all conditions to happen the transfer. Check event', function() {
                assert.equal(event,'Transfer', "ERROR: the event has to be @Transfer@");
            });

            
            it('06.09.2_transferFrom_from_AccFoundNDF3_to other account. There are all conditions to happen the transfer. Check balance of AccFoundNDF3', function() {
                     return MyERC20.balanceOf(AccFoundNDF3).then(function(callResult){    
                     //console.log('balanceAccFoundNDF3After=',callResult);
                     //console.log('balanceAcc0Before=',balanceAcc0Before);
                    
                    assert.equal(balanceAccFoundNDF3Before.minus(callResult),amount_transfer, "ERROR  of   token transfer");
                   return true;
                });
            });

            it('06.09.3_transferFrom_from_AccFoundNDF3_to other account. There are all conditions to happen the transfer. Check allowance', function() {
                     return MyERC20.allowance(AccFoundNDF3, accounts[0]).then(function(callResult){    
                    //console.log('allowance_after______________=',callResult);
                    //console.log('allowance_before______________=',allowance_before);
                    //return true;
                    assert.equal(allow_before.minus(callResult), amount_transfer, "ERROR  of   token transfer via call of method @transferFrom@");
                });
            });


    });

     describe('Account AccFoundNDF3 transfers the tokens. The transfer has to happen. There are all conditions for it.'
                +'\r\n Expected behaviour: The attempt is successful. The transfer has to happen', function(){
            it('06.10_transfer_from_AccFoundNDF3_account_to_other_account', function() {
  
  
                return MyERC20.balanceOf.call(AccFoundNDF3).then(function(callResult){
                    //console.log('balanceAccFoundNDF3Before',callResult.toNumber());
                    balanceAccFoundNDF3Before = callResult.toNumber();
                    return MyERC20.balanceOf.call(accounts[0]);
                }).then(function(callResult){
                    //console.log('balanceAcc0Before',callResult.toNumber());
                    balanceAcc0Before = callResult.toNumber();
                    return MyERC20.transfer(accounts[0],amount_transfer,{from: AccFoundNDF3, gasPrice: web3.toWei(1,"Gwei")});
                }).then(function(txResult){
                    return MyERC20.balanceOf(AccFoundNDF3);
                }).then(function(callResult){
                     //console.log('balanceAccFoundNDF3After=', callResult);
                    assert.equal(balanceAccFoundNDF3Before,callResult.add(amount_transfer), 'ERROR  of   token transfer');
                  //return true;
                });   
            }); 
    }); 


     describe('Account AccFoundNDF2 transfers the tokens. The transfer has to happen. There are all conditions for it.'
                +'\r\n Expected behaviour: The attempt is successful. The transfer has to happen', function(){
            it('06.11_transfer_from_AccFoundNDF2_account_to_other_account', function() {
  
  
                return MyERC20.balanceOf.call(AccFoundNDF2).then(function(callResult){
                    //console.log('balanceAccFoundNDF2Before',callResult.toNumber());
                    balanceAccFoundNDF2Before = callResult.toNumber();
                    return MyERC20.balanceOf.call(accounts[0]);
                }).then(function(callResult){
                    //console.log('balanceAcc0Before',callResult.toNumber());
                    balanceAcc0Before = callResult.toNumber();
                    return MyERC20.transfer(accounts[0],amount_transfer,{from: AccFoundNDF2, gasPrice: web3.toWei(1,"Gwei")});
                }).then(function(txResult){
                    return MyERC20.balanceOf(AccFoundNDF2);
                }).then(function(callResult){
                     //console.log('balanceAccFoundNDF2After=', callResult);
                    assert.equal(balanceAccFoundNDF2Before,callResult.add(amount_transfer), 'ERROR  of   token transfer');
                  //return true;
                });   
            }); 
    }); 


     describe('Account AccTeam transfers the tokens. The transfer has to happen. There are all conditions for it.'
                +'\r\n Expected behaviour: The attempt is successful. The transfer has to happen', function(){
            it('06.12_transfer_from_AccTeam_account_to_other_account', function() {
  
  
                return MyERC20.balanceOf.call(AccTeam).then(function(callResult){
                    //console.log('balanceAccTeamBefore',callResult.toNumber());
                    balanceAccTeamBefore = callResult.toNumber();
                    return MyERC20.balanceOf.call(accounts[0]);
                }).then(function(callResult){
                    //console.log('balanceAcc0Before',callResult.toNumber());
                    balanceAcc0Before = callResult.toNumber();
                    return MyERC20.transfer(accounts[0],amount_transfer,{from: AccTeam, gasPrice: web3.toWei(1,"Gwei")});
                }).then(function(txResult){
                    return MyERC20.balanceOf(AccTeam);
                }).then(function(callResult){
                     //console.log('balanceAccTeamAfter=', callResult);
                    assert.equal(balanceAccTeamBefore,callResult.add(amount_transfer), 'ERROR  of   token transfer');
                  //return true;
                });   
            }); 
    }); 



});