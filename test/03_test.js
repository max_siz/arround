var Arroundtoken = artifacts.require("./Arroundtoken.sol");
var MyERC20;
var gasUsedwei=0;
var gasUsed=0;
var _totalSupply;
var event;
var event1;
var owner_old;
var owner_new;
var cnt;
var AccTDE;
var amount_transfer=1000;
var balanceAcc8Before;
var balanceAcc8After;
var balanceAccTDEBefore;

contract('Arroundtoken functionality tests - 03',async (accounts) => {

    // body...
        //acc[1], //accTDE
        //acc[2], //_accFoundCDF
        //acc[3], //_accFoundNDF1
        //acc[4], //_accFoundNDF2
        //acc[5], //_accFoundNDF3
        //acc[6], //_accTeam
        //acc[7], //_accBounty

    before( function() {
        return Arroundtoken.deployed().then(function(instance){
            MyERC20 = instance;
            //have got count of tokens issue
            return MyERC20.totalSupply(); 
        }).then(function(callResult){
            _totalSupply=callResult;
            return MyERC20.accTDE.call();
        }).then(function(callResult){
            AccTDE=callResult;
            return true;
        });
    });

    describe('Account transfers the tokens to other account, there are enough   tokens '
                +'\r\n Expected behaviour: The attempt is successful. And the transfer has to happen', function(){
        it('03.01_transfer_from_AccTDE_account_to_other_account', function() {
  
  
            return MyERC20.balanceOf.call(accounts[8]).then(function(callResult){
                //console.log('balanceAcc8Before',callResult.toNumber());
                balanceAcc8Before = callResult.toNumber();
                return MyERC20.balanceOf.call(AccTDE);
            }).then(function(callResult){
                //console.log('balanceAccTDEBefore',callResult.toNumber());
                balanceAccTDEBefore = callResult.toNumber();
                return MyERC20.transfer(accounts[8],amount_transfer,{from: AccTDE, gasPrice: web3.toWei(1,"Gwei")});
            }).then(function(txResult){
                //console.log('transfer happened');
                event=txResult.logs.find(e => e.event === 'Transfer').event;
                return MyERC20.balanceOf(accounts[8]);

            }).then(function(callResult){
                balanceAcc8After=callResult;
                //console.log('balanceAcc8After=', callResult);
                assert.equal(callResult.minus(balanceAcc8Before)
                        ,amount_transfer, 'ERROR  of   token transfer');
            });   
        });
         


        it('03.01.01_transfer_from_AccTDE_account_to_other_account_Check_event', function() {
             assert.equal(event,'Transfer', "the event has to be @Transfer@");
                       
        });
  
        it('03.01.02_transfer_from_AccTDE_account_to_other_account_Check_balance of sender', function() {

            return MyERC20.balanceOf.call(AccTDE).then(function(callResult){
                //console.log('balanceAccTDEAfter',callResult);
                //balanceAcc8Before = callResult.toNumber();
                assert.equal(callResult.add(amount_transfer).minus(balanceAccTDEBefore),0,"ERROR: the transfer is incorrect!!!"); 
            });        
        });        
 
    });

    describe('Account tries to transfer the tokens to other when the recipient account is 0 or the sender account has not enough balance'
                +'\r\n Expected behaviour: The attempts are failed. And the transfers have not happen', function(){
        it('03.02_transfer_from_Acc8_to_0', function() {
  
  
                
                    return MyERC20.transfer('0x0000000000000000000000000000000000000000',amount_transfer,{from: accounts[8], gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                console.log("ERROR! - OK. The transfer has not happened.", err.toString());
              
            }).then(function(txResult){
                return MyERC20.balanceOf(accounts[8]);

            }).then(function(callResult){
                //console.log('balanceAcc8After=', callResult);
                //console.log('balanceAcc8Before=', balanceAcc8After);
                
                assert.equal(callResult.minus(balanceAcc8After),0
                        , 'ERROR: the balance has changed!!!');
            });   
        });
        

        it('03.03_transfer_the_tokens_Amount_of transfer is more than the balance', function() {
  
  
                
                    return MyERC20.transfer(accounts[9],amount_transfer+1,{from: accounts[8], gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                console.log("ERROR! - OK. The transfer has not happened.", err.toString());
              
            }).then(function(txResult){
                return MyERC20.balanceOf(accounts[8]);

            }).then(function(callResult){
                //console.log('balanceAcc8After=', callResult);
                //console.log('balanceAcc8Before=', balanceAcc8After);
                assert.equal(callResult.minus(balanceAcc8After),0
                        , 'the balance has changed!!!');
            });   
        });
 
    });


});







                  



   

