var Arroundtoken = artifacts.require("./Arroundtoken.sol");
var MyERC20;
var balanceAccTDEBefore;
var balanceAccASTONBefore;
var balanceAccFoundationBefore;
var balanceAccAdvisorsBefore;
var balanceAccBountyBefore;
var balanceAccTeam1Before;
var balanceAccTeam2Before;
var balanceAccTeam3Before;
var gasUsedwei=0;
var gasUsed=0;
var _totalSupply;
var res;
contract('Arroundtoken functionality tests - 02',function (accounts) {

     //     1     //accTDE
        // acc[2], //_accFoundCDF
        // acc[3], //_accFoundNDF
        // acc[4], //_accTeam
    //  // acc[5], //_accBounty
    // address public accTDE;
    // address public accFoundCDF;
    // address public accFoundNDF;
    // address public accTeam;
    // address public accBounty;

    before(function() {
        return Arroundtoken.deployed().then(function(instance){
            MyERC20 = instance;
            //have got count of tokens issue
            return MyERC20.totalSupply(); 
        }).then(function(callResult){
            _totalSupply=callResult;
            return true;
        });
    });

    describe('Checking of contract deployment : check the balances of service accounts.' 
                + '\r\n Expected behaviour: every service account has  a certain amount of tokens. It is token sale period ', function(){
          
    	it('02.01_check_tokens_of_TDE', function() {
            return MyERC20.accTDE.call().then(function(callResult){
                return MyERC20.balanceOf(callResult);
            }).then(function(callResult){
                balanceAccTDEBefore = callResult;

                //console.log('balanceAccTDEBefore=', balanceAccTDEBefore);
                assert.equal(balanceAccTDEBefore.toString(10),'1104000000000000000000000000',
                + '\r\n _totalSupply      =' +_totalSupply.toNumber()
                + '\r\n TDE balance       ='+  balanceAccTDEBefore 
                + '\r\n'
                );
                    
            });
        });
     
        it('02.02_check_tokens_of_accFoundCDF', function() {
            return MyERC20.accFoundCDF.call().then(function(callResult){
                return MyERC20.balanceOf(callResult);
            }).then(function(callResult){
                balanceAccASTONBefore = callResult;
                
                assert.equal(balanceAccASTONBefore.toString(10) ,'1251000000000000000000000000',
                    'accFoundCDF tokens must be '+callResult.toString()
                    + '\r\n _totalSupply                      =' +_totalSupply.toNumber()
                    + '\r\n accFoundCDF balance       ='+  balanceAccASTONBefore 
                    + '\r\n'
                    );
            });
        });

        it('02.03_check_tokens_of_accFoundNDF1', function() {
            return MyERC20.accFoundNDF1.call().then(function(callResult){
                return MyERC20.balanceOf(callResult);
            }).then(function(callResult){
                balanceAccASTONBefore = callResult;
                
                assert.equal(balanceAccASTONBefore.toString(10) ,'150000000000000000000000000',
                    'accFoundCDF tokens must be '+callResult.toString()
                    + '\r\n _totalSupply                      =' +_totalSupply.toNumber()
                    + '\r\n accFoundNDF balance       ='+  balanceAccASTONBefore 
                    + '\r\n'
                    );
            });
        });

        it('02.03_check_tokens_of_accFoundNDF2', function() {
            return MyERC20.accFoundNDF2.call().then(function(callResult){
                return MyERC20.balanceOf(callResult);
            }).then(function(callResult){
                balanceAccASTONBefore = callResult;
                
                assert.equal(balanceAccASTONBefore.toString(10) ,'105000000000000000000000000',
                    'accFoundCDF tokens must be '+callResult.toString()
                    + '\r\n _totalSupply                      =' +_totalSupply.toNumber()
                    + '\r\n accFoundNDF balance       ='+  balanceAccASTONBefore 
                    + '\r\n'
                    );
            });
        });

        it('02.03_check_tokens_of_accFoundNDF3', function() {
            return MyERC20.accFoundNDF3.call().then(function(callResult){
                return MyERC20.balanceOf(callResult);
            }).then(function(callResult){
                balanceAccASTONBefore = callResult;
                
                assert.equal(balanceAccASTONBefore.toString(10) ,'45000000000000000000000000',
                    'accFoundCDF tokens must be '+callResult.toString()
                    + '\r\n _totalSupply                      =' +_totalSupply.toNumber()
                    + '\r\n accFoundNDF balance       ='+  balanceAccASTONBefore 
                    + '\r\n'
                    );
            });
        });

        it('02.04_check_tokens_of_accTeam', function() {
            return MyERC20.accTeam.call().then(function(callResult){
                return MyERC20.balanceOf(callResult);
            }).then(function(callResult){
                balanceAccASTONBefore = callResult;
                
                assert.equal(balanceAccASTONBefore.toString(10) ,'300000000000000000000000000',
                    'accFoundCDF tokens must be '+callResult.toString()
                    + '\r\n _totalSupply          =' +_totalSupply.toNumber()
                    + '\r\n accTeam balance       ='+  balanceAccASTONBefore 
                    + '\r\n'
                    );
            });
        });

        it('02.05_check_tokens_of_accBounty', function() {
            return MyERC20.accBounty.call().then(function(callResult){
                return MyERC20.balanceOf(callResult);
            }).then(function(callResult){
                balanceAccASTONBefore = callResult;
                
                assert.equal(balanceAccASTONBefore.toString(10) ,'45000000000000000000000000',
                    'accFoundCDF tokens must be '+callResult.toString()
                    + '\r\n _totalSupply          =' +_totalSupply.toNumber()
                    + '\r\n accBounty balance       ='+  balanceAccASTONBefore 
                    + '\r\n'
                    );
            });
        });
    });   
});                