//const { latestTime } = require('./latestTime');


function getBonusPercent(stage, ether) {

  if (stage == 0) stageBonus = 0.3
  else if (stage == 1) stageBonus = 0.2
  else if (stage == 2) stageBonus = 0.15
  else if (stage == 3) stageBonus = 0.10    
  else if (stage == 4) stageBonus = 0.05    
  else  stageBonus = 0;

  if      (ether >= 2000 )
      ethBonus = 0.25;
    else if (ether >= 1000 )
      ethBonus = 0.20;
    else if (ether >= 500  )
      ethBonus = 0.15;
    else if (ether >= 300  )
      ethBonus = 0.09;
    else if (ether >= 100  )
      ethBonus = 0.07;
    else if (ether >= 50   )
      ethBonus = 0.05;
    else
      ethBonus = 0;
  res = stageBonus + ethBonus;
  if (res > 0.3) res = 0.3
  return res;
}


module.exports = {
  getBonusPercent,
};
