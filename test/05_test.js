var Arroundtoken = artifacts.require("./Arroundtoken.sol");
var MyERC20;
var gasUsedwei=0;
var gasUsed=0;
var _totalSupply;
var _TDE_FINISH;
var date_block;
var amount_transfer=10;
var balanceAcc9Before;
var balanceAcc9After;
var event;
var balanceAccTeamBefore;
var balanceAccFoundNDF2Before;
var balanceAccFoundNDF3Before;
var balanceAcc0Before;
var balanceAcc0After;
var AccTeam;
var AccFoundNDF2
var AccFoundNDF3
var allow_before;
var date_unfreezing;

var event;
var event1;


contract('Arroundtoken functionality tests - 05',async (accounts) => {

    
    // body...
        //acc[1], //accTDE
        //acc[2], //_accFoundCDF
        //acc[3], //_accFoundNDF1
        //acc[4], //_accFoundNDF2
        //acc[5], //_accFoundNDF3
        //acc[6], //_accTeam
        //acc[7], //_accBounty


    before( function() {
        return Arroundtoken.deployed().then(function(instance){
            MyERC20 = instance;
            //have got count of tokens issue
            return MyERC20.totalSupply(); 
        }).then(function(callResult){
            _totalSupply=callResult
            return MyERC20.TDE_FINISH.call();
        }).then(function(callResult) {
           // console.log('TDE= ', callResult.toNumber());
              _TDE_FINISH=callResult.toNumber();
             return MyERC20.accTeam.call();
        }).then(function(callResult) {
              AccTeam=callResult;
             return MyERC20.accFoundNDF2.call();
        }).then(function(callResult) {
              AccFoundNDF2=callResult;
              return MyERC20.accFoundNDF3.call();
        }).then(function(callResult) {
              AccFoundNDF3=callResult;
             return true;
        });
    });

    describe('Checking of tokens transfer from accountTeam during frozen time period.'
                +'\r\n Expected behaviour: The attempts is failed. The transfers have not to happen'
                +'\r\n  the balances of accounts are not changed.', function(){

            it('05.01.0_transfer_from_accTeam_to_other_account_during frozen time period_Search_sender_account in frozen accounts_Before', function() {
                   return MyERC20.frozenAccounts.call(AccTeam).then(function(callResult){
                        //console.log('date_unfreeze',callResult);
                        //console.log('_TDE_FINISH',_TDE_FINISH);
                        assert.isAbove(callResult.toNumber(),_TDE_FINISH,'ERROR: the account has to be unfrozen')
                      // return true;
                        
                    });
                });



            it('05.01.1_transfer_from_accTeam(it is frozen)_to_other_account', function() {
                    
                       return MyERC20.balanceOf(AccTeam).then(function(callResult){
                        //console.log('balance_AccTeam_before=',callResult);
                        balanceAccTeamBefore=callResult;
                        return MyERC20.balanceOf(accounts[0]);
                    }).then(function(callResult) {
                        //console.log('balance_acc0_before=', callResult)
                        balanceAcc0Before=callResult;
                        return MyERC20.transfer(accounts[0],amount_transfer,{from: AccTeam, gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                        console.log("ERROR! - OK, the transfer has not happened, it is the frozen time", err.toString())
                        });
                    }).then(function(txResult){
                         return MyERC20.balanceOf(AccTeam);
                    }).then(function(callResult){
                         //console.log('balance_AccTeam_After=',callResult);           
                        assert.equal(callResult.minus(balanceAccTeamBefore),0,"ERROR: the tokens has to be frozen ");
                      
                    });
                });

            it('05.01.2_transfer_from_accTeam(it is frozen)_to_other_account_Check_balance_of_recipient', function() {
                        
                        return MyERC20.balanceOf(accounts[0]).then(function(callResult){
                        //console.log('balance_acc0_after=', callResult);
                        //console.log('balance_acc0_before', balanceAcc0Before);
                         assert.equal(callResult-balanceAcc0Before,0,"ERROR: the balance has not to change");
                       
                    });
            });
    });

    describe('Checking of tokens transfer from AccFoundNDF2 during frozen time period.'
                +'\r\n Expected behaviour: The attempts is failed. The transfers have not to happen'
                +'\r\n  the balances of accounts are not changed.', function(){

            it('05.02.0_transfer_from_AccFoundNDF2_to_other_account_during frozen time period_Search_sender_account in frozen accounts_Before', function() {
                   return MyERC20.frozenAccounts.call(AccFoundNDF2).then(function(callResult){
                       // console.log('date_unfreeze',callResult);
                       // console.log('_TDE_FINISH',_TDE_FINISH);
                        assert.isAbove(callResult.toNumber(),_TDE_FINISH,'ERROR: the account has to be unfrozen')
                      // return true;
                        
                    });
                });



            it('05.02.1_transfer_from_AccFoundNDF2(it is frozen)_to_other_account', function() {
                    
                       return MyERC20.balanceOf(AccFoundNDF2).then(function(callResult){
                        //console.log('balance_AccFoundNDF2_before=',callResult);
                        balanceAccFoundNDF2Before=callResult;
                        return MyERC20.balanceOf(accounts[0]);
                    }).then(function(callResult) {
                        //console.log('balance_acc0_before=', callResult)
                        balanceAcc0Before=callResult;
                        return MyERC20.transfer(accounts[0],amount_transfer,{from: AccFoundNDF2, gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                        console.log("ERROR! - OK, the transfer has not happened, it is the frozen time", err.toString())
                        });
                    }).then(function(txResult){
                         return MyERC20.balanceOf(AccFoundNDF2);
                    }).then(function(callResult){
                         //console.log('balance_AccFoundNDF2_After=',callResult);           
                        assert.equal(callResult.minus(balanceAccFoundNDF2Before),0,"ERROR: the tokens has to be frozen ");
                      
                    });
                });

            it('05.02.2_transfer_from_AccFoundNDF2(it is frozen)_to_other_account_Check_balance_of_recipient', function() {
                        
                        return MyERC20.balanceOf(accounts[0]).then(function(callResult){
                        //console.log('balance_acc0_after=', callResult);
                        //console.log('balance_acc0_before', balanceAcc0Before);
                         assert.equal(callResult-balanceAcc0Before,0,"ERROR: the balance has not to change");
                       
                    });
            });
    });


    describe('Checking of tokens transfer from AccFoundNDF3 during frozen time period.'
                +'\r\n Expected behaviour: The attempts is failed. The transfers have not to happen'
                +'\r\n  the balances of accounts are not changed.', function(){

            it('05.03.0_transfer_from_AccFoundNDF3_to_other_account_during frozen time period_Search_sender_account in frozen accounts_Before', function() {
                   return MyERC20.frozenAccounts.call(AccFoundNDF3).then(function(callResult){
                       // console.log('date_unfreeze',callResult);
                       // console.log('_TDE_FINISH',_TDE_FINISH);
                        assert.isAbove(callResult.toNumber(),_TDE_FINISH,'ERROR: the account has to be unfrozen')
                      // return true;
                        
                    });
                });



            it('05.03.1_transfer_from_AccFoundNDF3(it is frozen)_to_other_account', function() {
                    
                       return MyERC20.balanceOf(AccFoundNDF3).then(function(callResult){
                        //console.log('balance_AccFoundNDF3_before=',callResult);
                        balanceAccFoundNDF3Before=callResult;
                        return MyERC20.balanceOf(accounts[0]);
                    }).then(function(callResult) {
                        //console.log('balance_acc0_before=', callResult)
                        balanceAcc0Before=callResult;
                        return MyERC20.transfer(accounts[0],amount_transfer,{from: AccFoundNDF3, gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                        console.log("ERROR! - OK, the transfer has not happened, it is the frozen time", err.toString())
                        });
                    }).then(function(txResult){
                         return MyERC20.balanceOf(AccFoundNDF3);
                    }).then(function(callResult){
                         //console.log('balance_AccFoundNDF3_After=',callResult);           
                        assert.equal(callResult.minus(balanceAccFoundNDF3Before),0,"ERROR: the tokens has to be frozen ");
                      
                    });
                });

            it('05.03.2_transfer_from_AccFoundNDF3(it is frozen)_to_other_account_Check_balance_of_recipient', function() {
                        
                        return MyERC20.balanceOf(accounts[0]).then(function(callResult){
                        //console.log('balance_acc0_after=', callResult);
                        //console.log('balance_acc0_before', balanceAcc0Before);
                         assert.equal(callResult-balanceAcc0Before,0,"ERROR: the balance has not to change");
                       
                    });
            });
    });



    describe('Account owner tries to increase allowance of using his own tokens when the account of token owner is frozen. ' 
                +'\r\n Expected behaviour: The attempt is failed. The allowance is not increased', function(){

        it('05.04.1_increase allowance during frozen time period', function() {
                   
                    return MyERC20.allowance(AccTeam, accounts[0]).then(function(callResult){
                    //console.log('allow_before=', callResult);
                    allow_before=callResult;
                    return MyERC20.increaseAllowance(accounts[0], 10 , {from: AccTeam, gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                    console.log("ERROR! - OK, the increasing of allowance did not work", err.toString())
                        });
                    }).then(function(){
                       return MyERC20.allowance(AccTeam, accounts[0]);
                    }).then(function(callResult){
                      //console.log('allow_after=', callResult);
                    assert.equal(callResult.minus(allow_before),0, "ERROR: the allowance has not to increase");
                   
                });
            });
    });
    

    describe('Account owner tries to decrease allowance of using  his own tokens when the account of token owner is frozen.'
                +'\r\n Expected behaviour: The attempt is failed. The allowance is not decreased', function(){

        it('05.05.1_decrease allowance during frozen time period', function() {
                   
                    return MyERC20.allowance(AccFoundNDF2, accounts[0]).then(function(callResult){
                    //console.log('allow_before=', callResult);
                    allow_before=callResult;
                    return MyERC20.decreaseAllowance(accounts[0], 1 , {from: AccFoundNDF2, gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                    console.log("ERROR! - OK, the decreasing of allowance did not work", err.toString())
                        });
                    }).then(function(){
                        return MyERC20.allowance(AccFoundNDF2, accounts[0]);
                    }).then(function(callResult){
                      //console.log('allow_after=', callResult);
                    assert.equal(allow_before.minus(callResult),0, "ERROR: the allowance has not to decrease");
                   
                });
            });
    });

    describe('Account owner tries to approve to use his own tokens when the account of token owner is frozen. '
                +'\r\n Expected behaviour: The attempt is failed. The allowance is not gotten', function(){

        it('05.06.1_approve to use the tokens during frozen time period', function() {
                   
                    return MyERC20.allowance(AccFoundNDF3, accounts[0]).then(function(callResult){
                    //console.log('allow_before=', callResult);
                    allow_before=callResult;
                    return MyERC20.approve(accounts[0], 1 , {from: AccFoundNDF3, gasPrice: web3.toWei(1,"Gwei")}).catch(function(err) {
                                    console.log("ERROR! - OK, the approval is not given", err.toString())
                        });
                    }).then(function(){
                        return MyERC20.allowance(AccFoundNDF3, accounts[0]);
                    }).then(function(callResult){
                    //console.log('allow_after=', callResult);
                    assert.equal(allow_before.minus(callResult),0, "ERROR: the approval has not to be given");
               
            });
        });
    });
});







                  



   

